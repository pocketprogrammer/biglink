<?php
require_once('vendor/autoload.php');
require_once('BlueLink/BlueLink_Config.php');
require_once('BlueLink/LizardLink.php');
require_once('BlueLink/InventoryService.php');
require_once('BlueLink/CustomerService.php');
require_once('BlueLink/OrderService.php');

use Bigcommerce\Api\Client as Bigcommerce;

class App {

    public $api_token = '761d3b3f1fc0e0296de220fe9ea26198f7e35973';
    protected $states = array(
        'Alabama'=>'AL',
        'Alaska'=>'AK',
        'Arizona'=>'AZ',
        'Arkansas'=>'AR',
        'California'=>'CA',
        'Colorado'=>'CO',
        'Connecticut'=>'CT',
        'Delaware'=>'DE',
        'Florida'=>'FL',
        'Georgia'=>'GA',
        'Hawaii'=>'HI',
        'Idaho'=>'ID',
        'Illinois'=>'IL',
        'Indiana'=>'IN',
        'Iowa'=>'IA',
        'Kansas'=>'KS',
        'Kentucky'=>'KY',
        'Louisiana'=>'LA',
        'Maine'=>'ME',
        'Maryland'=>'MD',
        'Massachusetts'=>'MA',
        'Michigan'=>'MI',
        'Minnesota'=>'MN',
        'Mississippi'=>'MS',
        'Missouri'=>'MO',
        'Montana'=>'MT',
        'Nebraska'=>'NE',
        'Nevada'=>'NV',
        'New Hampshire'=>'NH',
        'New Jersey'=>'NJ',
        'New Mexico'=>'NM',
        'New York'=>'NY',
        'North Carolina'=>'NC',
        'North Dakota'=>'ND',
        'Ohio'=>'OH',
        'Oklahoma'=>'OK',
        'Oregon'=>'OR',
        'Pennsylvania'=>'PA',
        'Rhode Island'=>'RI',
        'South Carolina'=>'SC',
        'South Dakota'=>'SD',
        'Tennessee'=>'TN',
        'Texas'=>'TX',
        'Utah'=>'UT',
        'Vermont'=>'VT',
        'Virginia'=>'VA',
        'Washington'=>'WA',
        'West Virginia'=>'WV',
        'Wisconsin'=>'WI',
        'Wyoming'=>'WY'
    );

    public function __construct()
    {

        $bigCommerce = new Bigcommerce();
        Bigcommerce::configure(array(
            'client_id' => 'dhfkupkdfazjrsz2ekbulxq0ias57t5',
            'auth_token' => '51gnt2iodrw2mc5zf5o22mm8mci7ms8',
            'store_hash' => '4pbfegf2yq',
            'client_secret' => 'j3wu4tpwev0cjji3t9bi0o5id11sh7m'
        ));
        
    }

    public function inventoryService()
    {
        return new InventoryService();
    }

    public function customerService()
    {
        return new CustomerService();
    }

    public function orderService()
    {
        return new OrderService();
    }

    public function getProducts($filter = null)
    {
        if($filter == null)
        {
            return Bigcommerce::getProducts();
        }
        else
        {
            if(!is_array($filter))
            {
                echo "Error: Filter must be of type Array".PHP_EOL;
                return false;
            }
            else
            {
                return Bigcommerce::getProducts($filter);
            }
        }
    }

    public function updateProductInventory($productId)
    {
        $product = Bigcommerce::getProduct($productId);
        $total_count = 0;
        if($product)
        {
            if(count($product->skus) < 1)
            {
                echo 'No variants for '.$product->sku.'</br>'.PHP_EOL;
                return false;
            }
            foreach($product->skus as $variant)
            {
                echo $variant->sku.': '.$variant->inventory_level.'</br>'.PHP_EOL;
                $inventory = $this->inventoryService()->getProductInventory($variant->sku);
                if($inventory != $variant->inventory_level && is_numeric($inventory))
                {
                    $variant->inventory_level = $inventory;
                    $variant->update();
                    echo 'Now: '.$variant->inventory_level.'</br>'.PHP_EOL;
                }
                $total_count += $variant->inventory_level;
            }
            if($total_count <= 0)
            {
                $product->availability = 'unavailable';
                $product->update();
                echo $product->sku.' made unavailable</br>'.PHP_EOL;
            }
            else
            {
                $product->inventory_level = $total_count;
                $product->availability = 'available';
                $product->update();
                echo $product->sku.' made available</br>'.PHP_EOL;
            }
        }
        return true;
    }

    public function getCustomer($customerId)
    {
        return Bigcommerce::getCustomer($customerId);
    }

    public function addCustomerShipToAddress($orderId,$addressId)
    {
        $customerService = $this->customerService();

        $order = $this->getOrders(["min_id"=>$orderId,"max_id"=>$orderId])[0];

        $address = $order->shipping_addresses[0];
        if($address->state == 'Florida') {
            $blueLinkCustomerId = 'BrokeDickT';
            $taxAuthority = 'FL';
        }
        else {
            $blueLinkCustomerId = 'BrokeDick';
            $taxAuthority = 'XX';
        }
        $customerService->customerAddShipTo(
            $blueLinkCustomerId,
            'BD'.$addressId,
            $address->first_name.' '.$address->last_name,
            mb_strimwidth($address->street_1,0,29),
            $address->street_2,
            $address->city,
            $this->states[$address->state],
            $address->zip,
            $address->country_iso2,
            $address->first_name.' '.$address->last_name,
            $address->phone,
            '',
            $taxAuthority
        );
        file_put_contents('ORDERLOGS.txt', date("Y-m-d H:i:s").": ShipTo added for order ".$orderId."\n",LOCK_EX | FILE_APPEND);

        return true;
    }

    public function getBlueLinkOrder($blueLinkOrderId)
    {
        $orderservice = $this->orderService();
        return $orderservice->getOrder($blueLinkOrderId);
    }
    
    public function getOrders($filter = null)
    {
        if($filter == null)
        {
            return Bigcommerce::getOrders();
        }
        else
        {
            if(!is_array($filter))
            {
                echo "Error: Filter must be of type Array".PHP_EOL;
                return false;
            }
            else
            {
                return Bigcommerce::getOrders($filter);
            }
        }
    }

    public function addSalesOrderToBlueLink($orderId)
    {
        $orderService = $this->orderService();

        $order = $this->getOrders(["min_id" => $orderId,"max_id" => $orderId])[0];

        if($order->shipping_addresses[0]->state == 'Florida')
        {
            $customerId = 'BrokeDickT';
        }
        else
        {
            $customerId = 'BrokeDick';
        }

        $customerShipTo = $this->addCustomerShipToAddress($order->id,$order->shipping_addresses[0]->id);
        if($customerShipTo != true)
        {
            file_put_contents('ORDERERRORS.txt',date("Y-m-d H:i:s").": Customer ShipTo not added for order '.$order->id.'... Halting.\n",LOCK_EX | FILE_APPEND);
            return false;
        }
        $blueLinkOrderId = $orderService->addSalesOrder($customerId,'BD'.$order->shipping_addresses[0]->id,$order->base_shipping_cost,$order->id,'Regular','USPS Priority','New');
        file_put_contents('ORDERLOGS.txt',date("Y-m-d H:i:s").": Order Header for ".$order->id." created in BlueLink (".$blueLinkOrderId.")\n",LOCK_EX | FILE_APPEND);

        if(is_numeric($blueLinkOrderId)) {
            $order->external_id = $blueLinkOrderId;
            $order->update();
        }
        
        foreach($order->products as $product)
        {
            $description = $product->name;
            if(count($product->product_options) > 0)
            {
                foreach($product->product_options as $option)
                {
                    if($option->display_name == 'Strength')
                    {
                        $description = $product->name.' '.$option->display_value;
                    }
                }
            }

            $total_item_discount = 0;
            if(count($product->applied_discounts) > 0)
            {
                foreach($product->applied_discounts as $applied_discount)
                {
                    $total_item_discount += $applied_discount->amount;
                }
            }
            if($product->base_price != 0 && $total_item_discount != 0)
                $discount_percent = ($total_item_discount/$product->base_price);
            else
                $discount_percent = 0;
            $orderService->addSalesOrderLine($blueLinkOrderId,$product->sku,$product->quantity,$description,$product->base_price,date('Y-m-d'),$discount_percent);
            file_put_contents('ORDERLOGS.txt',date("Y-m-d H:i:s").": (".$order->id.") Order line for ".$product->sku." created in BlueLink\n",LOCK_EX | FILE_APPEND);

        }

        return $blueLinkOrderId;
    }

    public function getHooks()
    {
        return Bigcommerce::listWebhook();
    }

    public function createHook($data)
    {
        $webhook = Bigcommerce::createWebhook($data);
        if(!$webhook) {
            $error = Bigcommerce::getLastError();
            return $error;
        }
        else
            return $webhook;

    }

    public function deleteHook($id)
    {
        return Bigcommerce::deleteWebhook($id);
    }
}
