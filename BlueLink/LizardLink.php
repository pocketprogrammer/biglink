<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 2/9/2017
 * Time: 5:48 PM
 */

class LizardLink {

    public function __construct($serviceType)
    {
        $clientOptions = [
            'soap_version' => BlueLink_Config::BL_CONFIG_VERSION,
            'cache_wsdl' => BlueLink_Config::BL_CONFIG_CACHE,
            'trace' => BlueLink_Config::BL_CONFIG_TRACE,
            'exceptions' => BlueLink_Config::BL_CONFIG_EXCEPTIONS
        ];

        switch($serviceType)
        {
            case 'inventory':   $serviceEndPoint = 'InventoryService'; break;
            case 'customer':    $serviceEndPoint = 'CustomerService'; break;
            case 'order':       $serviceEndPoint = 'OrderService'; break;
            case 'procedure':   $serviceEndPoint = 'StoredProcedure'; break;
            default: $serviceEndPoint = ''; break;
        }

        $wsdlPath = BlueLink_Config::BL_CONFIG_HOST.':'.BlueLink_Config::BL_CONFIG_PORT.'/'.$serviceEndPoint.'.svc?WSDL';
        $location = BlueLink_Config::BL_CONFIG_HOST.':'.BlueLink_Config::BL_CONFIG_PORT.'/'.$serviceEndPoint.'.svc/basic';

        $this->client = new SoapClient($wsdlPath,$clientOptions);
        if(BlueLink_Config::BL_CONFIG_VERSION == SOAP_1_1)
            $this->client->__setLocation($location);
    }
}