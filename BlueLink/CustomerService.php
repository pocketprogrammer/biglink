<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 2/9/2017
 * Time: 5:51 PM
 */

class CustomerService extends LizardLink {

    public function __construct()
    {
        parent::__construct('customer');
    }
    
    public function customerAddShipTo($customerId,
                                      $shipToCode,
                                      $shipName,
                                      $address,
                                      $address2,
                                      $city,
                                      $province,
                                      $postalCode,
                                      $country,
                                      $contact,
                                      $phone,
                                      $fax,
                                      $taxAuthority)
    {
        $data = [
            'webAppId' => BlueLink_Config::BL_CONFIG_KEY,
            'customerCode' => $customerId,
            'shipToCode' => $shipToCode,
            'shipName' => $shipName,
            'address1' => $address,
            'address2' => $address2,
            'city' => $city,
            'province' => $province,
            'postalCode' => $postalCode,
            'country' => $country,
            'contact' => $contact,
            'phone' => $phone,
            'fax' => $fax,
            'taxAuthority' => $taxAuthority
        ];

        try {
            $result = $this->client->CustomerShipToAdd($data)->CustomerShipToAddResult;
        }
        catch(\SoapFault $e)
        {
            $result = $e->getCode().": ".$e->getMessage();
            set_error_handler('var_dump', 0); // Never called because of empty mask.
            @trigger_error("");
            restore_error_handler();
        }
        return $result;
    }
}