<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 2/13/2017
 * Time: 10:40 AM
 */

class OrderService extends LizardLink {

    public function __construct()
    {
        parent::__construct('order');
    }

    public function getOrder($orderId)
    {
        $data = [
            'webAppId' => BlueLink_Config::BL_CONFIG_KEY,
            'salesOrder' => $orderId,
            'fetchAllHeaderColumns' => true,
            'fetchOrderDetails' => true,
            'fetchAllDetailColumns' => true
        ];

        $result = $this->client->GetOrder($data)->GetOrderResult;
        if($result == null)
        {
            return false;
        }
        return $result;
    }
    
    public function addSalesOrder(
        $customerId,
        $shipToCode,
        $freightAmount,
        $purchOrderRef = '',
        $orderType,
        $shipVia,
        $status
    )
    {
        $data = [
            'webAppId' => BlueLink_Config::BL_CONFIG_KEY,
            'customerCode' => $customerId,
            'shipToCode' => $shipToCode,
            'onlinePaymentPending' => 'false',
            'freightAmount' => $freightAmount,
            'location' => 'Main',
            'purchaseOrderReference' => $purchOrderRef,
            'jobCode' => '',
            'costCode' => '',
            'department' => '1012',
            'orderType' => $orderType,
            'shipVia' => $shipVia,
            'orderStatus' => $status
        ];

        $result = $this->client->SalesOrderCreate($data)->SalesOrderCreateResult;
        return $result;
    }

    public function addSalesOrderLine(
        $orderId,
        $productCode,
        $quantity,
        $productDescription,
        $price,
        $shipDate, //Timestamp (Today)
        $discount
    )
    {
        $data = [
            'webAppId' => BlueLink_Config::BL_CONFIG_KEY,
            'salesOrder' => $orderId,
            'productCode' => $productCode,
            'quantity' => $quantity,
            'UOM' => 'Each',
            'productDescription' => $productDescription,
            'price' => $price,
            'shipDate' => $shipDate,
            'jobCode' => '',
            'costCode' => '',
            'extendedNotes' => '',
            'misc1' => '',
            'misc2' => '',
            'parented' => 'false',
            'UBO' => '',
            'discount' => $discount
        ];
        $result = $this->client->SalesOrderDetailAdd($data)->SalesOrderDetailAddResult;
        return $result;
    }
}
