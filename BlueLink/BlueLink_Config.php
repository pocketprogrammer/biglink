<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 2/9/2017
 * Time: 5:47 PM
 */

class BlueLink_Config
{
    //const BL_CONFIG_KEY = 'DEB3E0AA-7242-4E03-9D2D-8E649D62C3BC';   // API Key (live)
    const BL_CONFIG_KEY = '77AA2FC4-0A55-4BD4-81D4-B9A2D40D135D';   // API Key (webtest)
    const BL_CONFIG_VERSION = SOAP_1_1;                             // SOAP Version
    const BL_CONFIG_CACHE = WSDL_CACHE_NONE;                        // WSDL Cache Mode
    const BL_CONFIG_TRACE = true;                                   // WSDL Stack Traces
    const BL_CONFIG_EXCEPTIONS = true;                              // WSDL Soapfaults
    const BL_CONFIG_HOST = 'http://saas.bluelinkerp.com';           // Webservices Host
    const BL_CONFIG_PORT = '37110';                                 // Webservices Port (WebTest)
    //const BL_CONFIG_PORT = '38110';                                 // Webservices Port (live)
}
