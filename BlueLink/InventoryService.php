<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 2/9/2017
 * Time: 5:53 PM
 */

class InventoryService extends LizardLink {

    public function __construct()
    {
        parent::__construct('inventory');
    }

    public function getProductInventory($productCode)
    {
        $data = [
            'webAppId' => BlueLink_Config::BL_CONFIG_KEY,
            'prodCode' => $productCode,
            'fetchInventoryUOM' => false,
            'fetchPriceInfo' => false
        ];
        return $this->client->GetInventoryAllColumns($data)->GetInventoryAllColumnsResult->Uoh;
    }
}