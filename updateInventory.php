<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 2/12/2017
 * Time: 2:08 PM
 */
require_once('app.php');

$app = new App();

foreach($app->getProducts() as $product)
{
    if($_GET['token'] != $app->api_token)
        http_response_code(404);
    else
        $app->updateProductInventory($product->id);
}
