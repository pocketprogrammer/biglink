<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 2/13/2017
 * Time: 4:55 PM
 */
require_once('app.php');
header('Content-Type: application/json');

$app = new App();
if($_GET['token'] != '761d3b3f1fc0e0296de220fe9ea26198f7e35973')
    http_response_code(404);
else {
    ob_start();
    $rawPostBody = file_get_contents('php://input');
    $postData = json_decode($rawPostBody, true);//$postData is now an array
    $order = $app->getOrders(["min_id"=>$postData['data']['id'],"max_id"=>$postData['data']['id']])[0];
    http_response_code(200); //set 200 response code to stop retry mechanism
    $size = ob_get_length();
    header("Content-Encoding: none");
    header("Content-Length: {$size}");
    header("Connection: close");
    ob_end_flush();
    ob_flush();
    flush();
    if(session_id()) session_write_close();

    $lastOrderCreatedAt = file_get_contents('lastordercreatedat');
    if($order && $order->order_source == 'manual')
    {
        file_put_contents('lastordercreatedat',$postData['created_at'],LOCK_EX);
    }
    
    if ($order && $postData['data']['status']['new_status_id'] == 11 && $lastOrderCreatedAt != $postData['created_at']) {
        file_put_contents('ORDERLOGS.txt', date("Y-m-d H:i:s").": Sending Order " . $order->id . " to BlueLink\n", LOCK_EX | FILE_APPEND);
        file_put_contents('ORDERLOGS.txt', date("Y-m-d H:i:s").": Status: ".$order->status."(".$order->status_id.")\n", LOCK_EX | FILE_APPEND);
        $app->addSalesOrderToBlueLink($order->id);
    }
}